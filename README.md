# guac-lite-server

Alternative client for Apache guacamole-server.

## Environment variables
| env                  | description                                                       | default     |
|----------------------|-------------------------------------------------------------------|-------------|
| BASEURL              | base URL                                                          | "/"         |
| CUSTOM_PORT          | http port                                                         | 3000        |
| KEYPATH              | path to private ssh key                                           | "./private" |
| SESSION_SECRET       | session secret for cookie                                         | -           |
| GUAC_LITE_AUTH_KEY   | used for basic auth for `/pubfor/`                                | -           |
| GUAC_ENCRYPTION_KEY  | key for encrypt and decrypt token (base64 encoded, 32 bytes long) | -           |
| AUTH_TYPE            | auth provider (cilogon/globus/keycloak)                           | "cilogon"   |
| OAUTH2_CLIENT_ID     | OAuth2 client ID                                                  | -           |
| OAUTH2_CLIENT_SECRET | OAuth2 client secret                                              | -           |
| KEYCLOAK_URL         | keycloak url (if AUTH_TYPE is keycloak)                           | -           |
| KEYCLOAK_REALM       | keycloak realm (if AUTH_TYPE is keycloak)                         | -           |

## Development
you can do development directly on the host with guac-lite-server as a standalone service,
or develop against a k8s cluster.

### Develop on the host

- install dependencies
```shell
npm install
```

- install dependencies for frontend
```shell
cd ./client
npm install
```

- create `.env` file from example and **modify** the config value
```shell
cp .env.example .env
```

- build front end code
```shell
cd ./client
npm run build
```

- disable auth if you are developing locally

comment out all lines in `requireAuth()` except `next()` in `server/index.js`
```js
function requireAuth(req, res, next) {
    //console.debug("authenticated:", req.isAuthenticated());
    //if (!req.isAuthenticated()) return res.redirect(`/login`);
    next();
}
```

- start up nodejs
```shell
# from root of the repo
npm run start
```

- start guacd
```shell
docker-compose up -d
```

### Develop on Kubernetes

Assume that all configmaps, secrets and persistent volume are set up.
```shell
skaffold run
```

### Running test
```shell
# from root of the repo
npm run test
```

### Generate encryption key
Generate a 32-bytes/256-bits key on the command line for `GUAC_ENCRYPTION_KEY`
```shell
cat /dev/random | head -c 32 | base64
```

### Generate token
You can generate a token using the helper in `./tokenutil/`
```shell
cd ./tokenutil
go build
./tokenutil --host <target_host_IP_address> --instance-username <username_on_target_host> --key <base64_encoded_key> --username <CACAO_username> --enable-sftp --sftp-root-dir /
```

## base url
If you set `BASEURL` for the backend to something other than `/`,
you also need to set the base url when building the front end,
see `client/src/App.js`, `process.env.REACT_APP_BASEURL`

e.g. if BASEURL is `/guacamole/`, `REACT_APP_BASEURL` needs to be `/guacamole`
