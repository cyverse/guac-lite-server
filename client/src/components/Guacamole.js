import React, { useEffect } from "react";
import Guacamole from "guacamole-common-js";

const drawerWidth = 250; // this is the width of the SideBar, TODO consider import this value rather than hard code.
const topBarHeight = 0;

const STATE_CONNECTED = 3;
const STATE_DISCONNECTED = 5;

export const GuacamoleContext = React.createContext();

export class GuacamoleWrapper {
    constructor(params) {
        this.token = params.token;
        const tunnel = new Guacamole.WebSocketTunnel(webSocketURL());
        this.client = new Guacamole.Client(tunnel);

        // Disconnect on close
        window.onunload = () => {
            this.client.disconnect();
        };

        // Mouse
        let mouse = new Guacamole.Mouse(this.client.getDisplay().getElement());
        mouse.onmousedown =
            mouse.onmouseup =
            mouse.onmousemove =
                (mouseState) => {
                    this.client.sendMouseState(mouseState, true);
                };
        this.mouse = mouse;

        // Touchscreen
        let touch = new Guacamole.Mouse.Touchscreen(this.client.getDisplay().getElement());
        touch.onmousedown =
            touch.onmouseup =
            touch.onmousemove =
                (mouseState) => {
                    this.client.sendMouseState(mouseState);
                };
        this.touch = touch;

        this.keyboard = new Guacamole.Keyboard(document);
        this.captureKeyboard();

        const setFileSystem = (fs) => {
            console.log("setFileSystem");
            this.filesystem = fs;
        };
        this.client.onfilesystem = function fileSystemReceived(object, name) {
            setFileSystem(object);
        };
        this.currentDirectory = getFileSystemRoot();

        // scale the display element when the window is resized.
        const updateSize = () => {
            const ratio = this.computeScaleRatio();
            const currScale = this.client.getDisplay().getScale();
            // note that scale() is happening locally in browser via css changes, this is not a rerender from server side.
            this.client.getDisplay().scale(currScale * ratio);
        };
        window.addEventListener("resize", updateSize);

        // scale the display element when the resolution is changed.
        this.client.getDisplay().onresize = (_width, _height) => {
            const ratio = this.computeScaleRatio();
            const currScale = this.client.getDisplay().getScale();
            // note that scale() is happening locally in browser via css changes, this is not a rerender from server side.
            this.client.getDisplay().scale(currScale * ratio);
        };

        const [width, height] = getDisplaySize();
        console.log(`display size: ${width}X${height}`);
        this.client.connect(`token=${this.token}&width=${width}&height=${height}`);
    }
    captureKeyboard() {
        if (!this.client || !this.keyboard) {
            return;
        }
        this.keyboard.onkeydown = (keysym) => {
            this.client.sendKeyEvent(1, keysym); // must be 1, true does not work
        };
        this.keyboard.onkeyup = (keysym) => {
            this.client.sendKeyEvent(0, keysym); // must be 0, false does not work
        };
    }
    stopCaptureKeyboard() {
        if (!this.keyboard) {
            return;
        }
        this.keyboard.onkeydown = null;
        this.keyboard.onkeyup = null;
    }

    /**
     * Set a callback function for receiving clipboard value.
     * Callback function will be called when the clipboard value is updated remotely.
     * @param receiverFunc
     */
    setClipboardReceiver(receiverFunc) {
        this.client.onclipboard = function clientClipboardReceived(stream, mimetype) {
            // Only process plain text data, ignore all other MIME
            if (isTextMIME(mimetype)) {
                let reader = new Guacamole.StringReader(stream);
                reader.ontext = function textReceived(text) {
                    receiverFunc(text);
                };
            }
        };
    }

    updateRemoteClipboardValue(value) {
        const clipboardStream = this.client.createClipboardStream("text/plain");
        let writer = new Guacamole.StringWriter(clipboardStream);
        writer.sendText(value);
        writer.sendEnd();
    }

    uploadFile(remoteFilePath, fileContent) {
        return new Promise((resolve, reject) => {
            if (!remoteFilePath || !fileContent) {
                reject(new Error("BUG, remoteFilePath or fileContent not set"));
                return;
            }
            if (typeof remoteFilePath !== "string") {
                reject(new Error("BUG, remoteFilePath is not string"));
                return;
            }
            const stream = this.client.createFileStream("application/octet-stream", remoteFilePath);
            const writer = new Guacamole.BlobWriter(stream);
            writer.sendBlob(fileContent);
            writer.oncomplete = function (_) {
                writer.sendEnd();
                console.log(`file uploaded, ${remoteFilePath}`);
                resolve();
            };
            writer.onerror = function (_, offset, err) {
                console.log(`file upload errored, ${remoteFilePath} ${offset} ${err}`);
                writer.sendEnd();
                reject(err);
            };
            console.log(`file upload start, ${remoteFilePath} ${fileContent.length}`);
        });
    }

    refreshFileSystem(file, callback) {
        return new Promise((resolve, reject) => {
            if (!this.filesystem) {
                reject(Error("filesystem not set"));
                return;
            }
            if (file.mimetype !== Guacamole.Object.STREAM_INDEX_MIMETYPE) {
                reject(Error("cannot refresh non-directory file"));
            }
            this.filesystem.requestInputStream(file.streamName, (stream, mimeType) => {
                // Ignore stream if mimetype is wrong
                if (mimeType !== Guacamole.Object.STREAM_INDEX_MIMETYPE) {
                    stream.sendAck("Unexpected mimetype", Guacamole.Status.Code.UNSUPPORTED);
                    return;
                }

                // Signal server that data is ready to be received
                stream.sendAck("Ready", Guacamole.Status.Code.SUCCESS);

                // Read stream as JSON
                var reader = new Guacamole.JSONReader(stream);

                // Acknowledge received JSON blobs
                reader.onprogress = function onprogress() {
                    stream.sendAck("Received", Guacamole.Status.Code.SUCCESS);
                };

                // Reset contents of directory
                reader.onend = function jsonReady() {
                    async function updateFileContents() {
                        // Empty contents
                        file.files = {};

                        // Determine the expected filename prefix of each stream
                        let expectedPrefix = file.streamName;
                        if (expectedPrefix.charAt(expectedPrefix.length - 1) !== "/")
                            expectedPrefix += "/";

                        // For each received stream name
                        let mimetypes = reader.getJSON();
                        for (const name in mimetypes) {
                            // Assert prefix is correct
                            if (name.substring(0, expectedPrefix.length) !== expectedPrefix)
                                continue;

                            // Extract filename from stream name
                            let filename = name.substring(expectedPrefix.length);

                            // Add file entry
                            file.files[filename] = {
                                mimetype: mimetypes[name],
                                streamName: name,
                                parent: file,
                                name: filename,
                            };
                        }
                    }
                    updateFileContents().then(() => {
                        console.log("updateFileContents() finished");
                        callback();
                        resolve();
                    });
                };
            });
        });
    }

    refreshFileSystemCurrentDirectory(callback) {
        return this.refreshFileSystem(this.currentDirectory, () => {
            callback(this.currentDirectory);
        });
    }

    changeDirectoryAndRefresh(targetPath, callback) {
        if (!targetPath) {
            return;
        }
        let newPath = Guacamole.Object.ROOT_STREAM;
        if (newPath[newPath.length - 1] !== "/") {
            newPath = newPath + "/";
        }
        if (targetPath && targetPath[0] === "/") {
            newPath = newPath + targetPath.substring(1);
        } else {
            newPath = newPath + targetPath;
        }

        this.currentDirectory = {
            mimetype: Guacamole.Object.STREAM_INDEX_MIMETYPE,
            streamName: newPath,
        };
        return this.refreshFileSystem(this.currentDirectory, () => {
            callback(this.currentDirectory);
        });
    }

    async downloadFile(targetPath) {
        if (!this.filesystem) {
            return;
        }
        if (!targetPath) {
            return new Error("BUG, remoteFilePath is not set");
        }
        if (typeof targetPath !== "string") {
            return new Error("BUG, remoteFilePath is not string");
        }
        let newPath = Guacamole.Object.ROOT_STREAM;
        if (newPath[newPath.length - 1] !== "/") {
            newPath = newPath + "/";
        }
        if (targetPath && targetPath[0] === "/") {
            newPath = newPath + targetPath.substring(1);
        } else {
            newPath = newPath + targetPath;
        }
        console.log("newPath", newPath);

        // const tunnelUUID = this.tunnel.uuid;
        this.filesystem.requestInputStream(newPath, (stream, mimetype) => {
            // Parse filename from string
            const filename = targetPath.match(/(.*[\\/])?(.*)/)[2];

            stream.sendAck("Ready", Guacamole.Status.Code.SUCCESS);
            console.log("mime", mimetype);

            let prevLength = 0;
            let reader = new Guacamole.BlobReader(stream, mimetype);
            reader.onprogress = function onprogress(length) {
                if (length - prevLength > 1024 * 10) {
                    console.log("reader.onprogress", length);
                    prevLength = length;
                }
                console.log("reader.onprogress", length);
                stream.sendAck("Received", Guacamole.Status.Code.SUCCESS);
            };
            reader.onend = function () {
                console.log("file download, data transfer,", reader.getLength(), reader.getBlob());
                downloadFile(filename, mimetype, reader.getBlob());
            };
        });
    }

    // compute a ratio to scaling (0~1.0 to shrink, >1.0 to enlarge) that will make the display element fill the available area.
    computeScaleRatio() {
        if (
            this.client.getDisplay().getElement().clientWidth === 0 ||
            this.client.getDisplay().getElement().clientHeight === 0
        ) {
            return 1.0;
        }

        // knowing drawerWidth, compute the leftover width
        const availableWidth = window.innerWidth - drawerWidth - 10;
        const availableHeight = window.innerHeight - topBarHeight - 20;

        const currentWidth = this.client.getDisplay().getElement().clientWidth;
        const currentHeight = this.client.getDisplay().getElement().clientHeight;

        const widthRatio = availableWidth / currentWidth;
        const heightRatio = availableHeight / currentHeight;
        return widthRatio > heightRatio ? heightRatio : widthRatio;
    }

    close() {
        this.client.disconnect();
    }
}

function getFileSystemRoot() {
    return {
        mimetype: Guacamole.Object.STREAM_INDEX_MIMETYPE,
        streamName: Guacamole.Object.ROOT_STREAM,
    };
}

function getDisplaySize() {
    // this is only useful for SSH connection. for VNC connection, the display resolution is dictated by DE(Desktop Environment).
    let width = window.innerWidth - drawerWidth;
    let height = window.innerHeight - topBarHeight;
    return [width, height];
}

function isTextMIME(mimetype) {
    // match any mime start with "text/", e.g. "text/plain", "text/csv", "text/css"
    return /^text\//.exec(mimetype);
}

export function GuacamoleStage() {
    const guacWrapper = React.useContext(GuacamoleContext);

    const myRef = React.createRef();
    useEffect(() => {
        // Add the guacamole element to current ref
        myRef.current.appendChild(guacWrapper.client.getDisplay().getElement());

        // Replace guacamole element with text upon disconnect
        guacWrapper.client.onstatechange = function (params) {
            if (!myRef.current) {
                return;
            }
            if (params === STATE_DISCONNECTED) {
                console.log("disconnected");
                // remove guacamole element
                myRef.current.removeChild(guacWrapper.client.getDisplay().getElement());

                // replace with text
                const note = document.createElement("h3");
                note.textContent = "disconnected!";
                myRef.current.appendChild(note);

                guacWrapper.stopCaptureKeyboard();
            } else if (params === STATE_CONNECTED) {
                console.log("connected");
            }
        };
    });
    return <div ref={myRef} />;
}

function webSocketURL() {
    let wsProtocol;
    if (window.location.protocol === "https:") {
        wsProtocol = "wss:";
    } else {
        wsProtocol = "ws:";
    }
    let wsURL = new URL(window.location.href);
    wsURL.protocol = wsProtocol;
    if (wsURL.pathname[wsURL.pathname.length - 1] !== "/") {
        wsURL.pathname += "/";
    }
    wsURL.pathname += "guaclite"; // the sub-path must match what is declared on the server side for guac-lite

    wsURL.search = ""; // clear query parameters, query parameters are added later on w/ this.client.connect()
    return wsURL.href;
}

function downloadFile(filename, mimeType, content) {
    const a = document.createElement("a");
    const blob = new Blob([content], { type: mimeType });
    const url = URL.createObjectURL(blob);
    console.log(url);
    a.setAttribute("href", url);
    a.setAttribute("download", filename);
    a.style.display = "none"; // hides the element
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
}
