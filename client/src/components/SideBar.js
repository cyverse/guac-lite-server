import * as React from "react";
import { styled, useTheme } from "@mui/material/styles";
import Drawer from "@mui/material/Drawer";
import { Box, Divider, FormControl, Tooltip } from "@mui/material/";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import ClipboardIcon from "@mui/icons-material/ContentPaste";
import UploadIcon from "@mui/icons-material/Upload";
import DownloadIcon from "@mui/icons-material/Download";
import RefreshIcon from "@mui/icons-material/Refresh";
import FileBrowserIcon from "@mui/icons-material/FolderOpenOutlined";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import FolderIcon from "@mui/icons-material/Folder";
import FileIcon from "@mui/icons-material/InsertDriveFile";
import FolderCopyOutlinedIcon from "@mui/icons-material/FolderCopyOutlined";

const drawerWidth = 250;

const DrawerHeader = styled("div")(({ theme }) => ({
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
}));

export default function SideBar(props) {
    const theme = useTheme();

    return (
        <Drawer
            variant="permanent"
            sx={{
                width: drawerWidth,
                flexShrink: 0,
                "& .MuiDrawer-paper": {
                    width: drawerWidth,
                    boxSizing: "border-box",
                },
            }}
        >
            <DrawerHeader>
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    Cacao WebShell / WebDesktop
                </Typography>
            </DrawerHeader>
            <Box sx={{ maxWidth: "99%", margin: "0 auto" }}>
                <Box sx={{ margin: ".5rem" }}>
                    <ClipboardUI
                        clipboardValue={props.clipboardValue}
                        onClipboardValueChange={props.onClipboardValueChange}
                    />
                    {props.connectionType === "ssh" ? (
                        // File upload and file browser are only available for SSH connection since they are based on SFTP under the hood
                        <Box>
                            <Divider sx={{ marginY: ".5rem" }} />
                            <FileUploadUI
                                handleUploadFileButtonClicked={props.handleUploadFileButtonClicked}
                            />
                            <Divider sx={{ marginY: ".5rem" }} mt={2} />
                            <FileDownloadUI downloadFile={props.downloadFile} />
                            <Divider sx={{ marginY: ".5rem" }} mt={2} />
                            <FileSystemUI
                                openFileSystem={props.openFileSystem}
                                changeDirectoryAndRefresh={props.changeDirectoryAndRefresh}
                            />
                        </Box>
                    ) : null}
                </Box>
            </Box>
        </Drawer>
    );
}

function ClipboardUI(props) {
    return (
        <Stack spacing={2}>
            <Stack direction="row" justifyContent="center">
                <ClipboardIcon />
                <Typography>Clipboard</Typography>
            </Stack>
            <Box sx={{ marginBottom: "1rem" }}>
                <TextField
                    id="outlined-multiline-static"
                    //label="Clipboard"
                    multiline
                    rows={5}
                    placeholder="Copy content in and out of WebShell/WebDesktop"
                    value={props.clipboardValue}
                    onChange={props.onClipboardValueChange}
                />
            </Box>
        </Stack>
    );
}

const MAX_FILE_SIZE_BYTES = 10 * 1024 * 1024; // 10MB

function FileUploadUI(props) {
    const [uploadFilePath, setUploadFilePath] = React.useState("uploaded_file_12345");
    const [selectedFile, setSelectedFile] = React.useState(null);
    const handleFileChange = (event) => {
        if (!event.target.files) {
            return;
        }
        const file = event.target.files[0];
        if (file?.size && file.size > MAX_FILE_SIZE_BYTES) {
            // this only limits the size on frontend, which is not bullet-proof.
            alert("cannot upload more than 10MB");
            return;
        }
        console.log(file);
        setSelectedFile(file);
    };
    return (
        <Stack spacing={2}>
            <Tooltip title={"Upload to ~/" + uploadFilePath} arrow>
                <TextField
                    value={uploadFilePath}
                    onChange={(event) => {
                        setUploadFilePath(event.target.value);
                    }}
                />
            </Tooltip>
            <FormControl sx={{ marginTop: ".4rem" }}>
                <input type="file" name="file" onChange={handleFileChange} />
            </FormControl>
            <Button
                mt={2}
                variant="contained"
                startIcon={<UploadIcon />}
                onClick={() => {
                    props.handleUploadFileButtonClicked(uploadFilePath, selectedFile);
                }}
            >
                Upload File
            </Button>
        </Stack>
    );
}

function FileDownloadUI(props) {
    const [downloadFilePath, setDownloadFilePath] = React.useState("/opt/data.txt");
    return (
        <Stack spacing={2}>
            <Tooltip title="Download file path" arrow>
                <TextField
                    value={downloadFilePath}
                    onChange={(event) => {
                        setDownloadFilePath(event.target.value);
                    }}
                />
            </Tooltip>
            <Button
                mt={2}
                variant="contained"
                startIcon={<DownloadIcon />}
                onClick={() => {
                    console.log(downloadFilePath);
                    props.downloadFile(downloadFilePath);
                }}
            >
                Download File
            </Button>
        </Stack>
    );
}

function FileSystemUI(props) {
    const [changeDirectoryTarget, setChangeDirectoryTarget] = React.useState("/");
    const [fileSystemRoot, setFileSystemRoot] = React.useState({ files: {} });

    return (
        <Stack spacing={2} mt={2}>
            <Button variant="contained" startIcon={<FileBrowserIcon />}>
                File Browser
            </Button>
            <Button
                variant="contained"
                startIcon={<RefreshIcon />}
                onClick={function (_) {
                    props.openFileSystem((fsRoot) => {
                        setFileSystemRoot(fsRoot);
                    });
                }}
            >
                Refresh File System
            </Button>
            <FormControl>
                <Tooltip title="Current directory">
                    <TextField
                        value={changeDirectoryTarget}
                        onChange={(event) => {
                            setChangeDirectoryTarget(event.target.value);
                        }}
                        helperText="Navigate by command-line"
                    />
                </Tooltip>
            </FormControl>
            <Tooltip title="System files will appear below">
                <Button
                    variant="contained"
                    startIcon={<FolderCopyOutlinedIcon />}
                    onClick={function (_) {
                        props.changeDirectoryAndRefresh(changeDirectoryTarget, (fsRoot) => {
                            setFileSystemRoot(fsRoot);
                        });
                    }}
                >
                    View Directory
                </Button>
            </Tooltip>
            <Divider sx={{ marginY: ".5rem" }} />
            <Box> {renderFileSystemFiles(fileSystemRoot)} </Box>
        </Stack>
    );
}

function renderFileSystemFiles(fileSystemRoot) {
    if (!fileSystemRoot?.files) {
        return <List dense={true} />;
    }
    const folderMIME = "application/vnd.glyptodon.guacamole.stream-index+json";
    const fileEntries = Object.entries(fileSystemRoot.files);
    return (
        <List dense={true} disablePadding={true}>
            {fileEntries.map((entry, index, _) => {
                const [key, value] = entry;
                console.log(entry);
                return (
                    <ListItem>
                        <ListItemIcon>
                            {value.mimetype === folderMIME ? <FolderIcon /> : <FileIcon />}
                        </ListItemIcon>
                        <ListItemText
                            primary={<Typography variant="body2">{value?.name}</Typography>}
                            secondary={
                                <Typography variant="caption" sx={{ fontSize: ".6em" }}>
                                    {value?.mimetype}
                                </Typography>
                            }
                        />
                    </ListItem>
                );
            })}
        </List>
    );
}
