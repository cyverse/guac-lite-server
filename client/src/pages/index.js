import React from "react";
import { useLocation } from "react-router-dom";
import { GuacamoleStage, GuacamoleContext, GuacamoleWrapper } from "../components/Guacamole";
import SideBar from "../components/SideBar";
import Box from "@mui/material/Box";

function useQuery() {
    const { search } = useLocation();
    return React.useMemo(() => new URLSearchParams(search), [search]);
}

const MAX_FILE_SIZE_BYTES = 10 * 1024 * 1024; // 10MB

function Home() {
    let query = useQuery();
    let token = query.get("token");
    let connectionType = query.get("type") || "";

    const guacWrapper = new GuacamoleWrapper({ token: token });

    return (
        <GuacamoleContext.Provider value={guacWrapper}>
            <HomeInner connectionType={connectionType} />
        </GuacamoleContext.Provider>
    );
}

function HomeInner(props) {
    const [clipboardTextFieldValue, setClipboardTextFieldValue] = React.useState("");

    const guacWrapper = React.useContext(GuacamoleContext);

    const handleClipboardTextFieldChange = (event) => {
        setClipboardTextFieldValue(event.target.value);
        guacWrapper.updateRemoteClipboardValue(event.target.value);
        event.stopPropagation();
    };

    if (!guacWrapper) {
        // only render when guacamole client is initialized
        return <div />;
    }
    guacWrapper.setClipboardReceiver((val) => {
        setClipboardTextFieldValue(val);
    });

    return (
        <Box sx={{ display: "flex" }}>
            <div
                onClick={(_) => {
                    guacWrapper.stopCaptureKeyboard();
                    console.log("sidebar clicked");
                }}
            >
                <SideBar
                    connectionType={props.connectionType}
                    // clipboard
                    clipboardValue={clipboardTextFieldValue}
                    onClipboardValueChange={handleClipboardTextFieldChange}
                    // file upload
                    handleUploadFileButtonClicked={(path, selectedFile) => {
                        guacWrapper.uploadFile(path, selectedFile);
                    }}
                    // file download
                    downloadFile={(path) => {
                        guacWrapper.downloadFile(path);
                    }}
                    // file system
                    openFileSystem={(callback) => {
                        guacWrapper.refreshFileSystemCurrentDirectory(callback);
                    }}
                    changeDirectoryAndRefresh={(targetPath, callback) => {
                        guacWrapper.changeDirectoryAndRefresh(targetPath, callback);
                    }}
                />
            </div>
            <Box component="main">
                <div
                    onClick={(_) => {
                        guacWrapper.captureKeyboard();
                    }}
                    style={{ paddingLeft: "1em" }}
                >
                    <GuacamoleStage />
                </div>
            </Box>
        </Box>
    );
}

export default Home;
