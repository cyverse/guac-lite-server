package main

import (
	"encoding/base64"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "tokenutil",
	Short: "Generate a token for connect to guac-lite-server",
	Long: `Generate an token that contains connection settings that is AES-256-CBC 
encrypted with optional HMAC on cipher text`,
	RunE: cmdRun,
}

var config struct {
	Conn       GuacamoleLiteConnection
	EnableSFTP bool
	IgnoreCert bool

	EncryptionKeyBase64   string
	EncryptionKeyFilename string
	HMACKeyBase64         string
	HMACKeyFilename       string
}

func init() {
	rootCmd.Flags().StringVar(&config.Conn.Type, "type", "ssh", "connection type, ssh/vnc/rdp")
	rootCmd.Flags().StringVar(&config.Conn.Settings.Hostname, "host", "localhost", "hostname of target server to connect to")
	rootCmd.Flags().StringVar(&config.Conn.Settings.Port, "port", "22", "the port to connect to on the target server")
	rootCmd.Flags().StringVar(&config.Conn.Settings.Username, "username", "", "username")
	rootCmd.Flags().StringVar(&config.Conn.Settings.InstanceUsername, "instance-username", "", "the username on the target server to connect to")
	rootCmd.Flags().StringVar(&config.Conn.Settings.PrivateKey, "private-key", "", "private ssh key")
	rootCmd.Flags().StringVar(&config.Conn.Settings.Password, "password", "", "VNC/RDP password")
	rootCmd.Flags().StringVar(&config.Conn.Settings.Security, "security", "tls", "RDP security, any, nla, nla-next, tls, vmconnect, rdp")
	rootCmd.Flags().BoolVar(&config.IgnoreCert, "ignore-cert", false, "Ignore cert, RDP TLS")
	rootCmd.Flags().BoolVar(&config.EnableSFTP, "enable-sftp", false, "Enable sftp")
	rootCmd.Flags().StringVar(&config.Conn.Settings.SFTPRootDirectory, "sftp-root-dir", "/", "SFTP root directory")

	rootCmd.Flags().StringVar(&config.EncryptionKeyBase64, "key", "", "encryption key (base64 encoded) to encrypt the token")
	rootCmd.Flags().StringVar(&config.EncryptionKeyFilename, "key-file", "", "file that contains encryption key (base64 encoded) to encrypt the token")
	rootCmd.Flags().StringVar(&config.HMACKeyBase64, "hmac-key", "", "key (base64 encoded) used to generate HMAC for the cipher text")
	rootCmd.Flags().StringVar(&config.HMACKeyFilename, "hmac-key-file", "", "file that contains key (base64 encoded) used to generate HMAC for the cipher text")
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func cmdRun(cmd *cobra.Command, args []string) error {

	switch config.Conn.Type {
	case "ssh":
	case "vnc":
	case "rdp":
	default:
		return fmt.Errorf("\"%s\" is not valid connection type", config.Conn.Type)
	}

	if config.Conn.Type == "ssh" && config.EnableSFTP {
		config.Conn.Settings.EnableSFTP = &config.EnableSFTP
		disableSFTPDownload := 0
		config.Conn.Settings.DisableSFTPDownload = &disableSFTPDownload
		disableSFTPUpload := 0
		config.Conn.Settings.DisableSFTPUpload = &disableSFTPUpload
	}

	if config.Conn.Type == "rdp" {
		if config.IgnoreCert {
			config.Conn.Settings.IgnoreCert = &config.IgnoreCert
		}
		config.Conn.Settings.SFTPRootDirectory = ""
	} else {
		config.Conn.Settings.Security = ""
	}

	key, err := encryptionKeyFromConfig()
	if err != nil {
		return err
	}

	hmacKey, err := hmacKeyFromConfig(false)
	if err != nil {
		return err
	}

	token, err := GenerateToken(&config.Conn, key, hmacKey)
	if err != nil {
		return err
	}
	fmt.Println(token)
	return nil
}

func encryptionKeyFromConfig() ([]byte, error) {
	if config.EncryptionKeyFilename == "" && config.EncryptionKeyBase64 == "" {
		return nil, fmt.Errorf("missing encryption key")
	}
	if config.EncryptionKeyBase64 != "" {
		key, err := base64.StdEncoding.DecodeString(config.EncryptionKeyBase64)
		if err != nil {
			return nil, err
		}
		if len(key) != 32 {
			return nil, fmt.Errorf("encryption key is not 32-bytes long")
		}
		return key, nil
	}
	content, err := os.ReadFile(config.EncryptionKeyFilename)
	if err != nil {
		return nil, err
	}
	key, err := base64.StdEncoding.DecodeString(string(content))
	if err != nil {
		return nil, err
	}
	if len(key) != 32 {
		return nil, fmt.Errorf("encryption key is not 32-bytes long")
	}
	return key, nil
}

func hmacKeyFromConfig(required bool) ([]byte, error) {
	if config.HMACKeyFilename == "" && config.HMACKeyBase64 == "" {
		if required {
			return nil, fmt.Errorf("missing hmac key")
		} else {
			return nil, nil
		}
	}
	if config.HMACKeyBase64 != "" {
		key, err := base64.StdEncoding.DecodeString(config.HMACKeyBase64)
		if err != nil {
			return nil, err
		}
		if len(key) <= 32 {
			return nil, fmt.Errorf("hmac key needs to be longer than 32-bytes")
		}
		return key, nil
	}
	content, err := os.ReadFile(config.HMACKeyFilename)
	if err != nil {
		return nil, err
	}
	key, err := base64.StdEncoding.DecodeString(string(content))
	if err != nil {
		return nil, err
	}
	if len(key) <= 32 {
		return nil, fmt.Errorf("hmac key needs to be longer than 32-bytes")
	}
	return key, nil
}

func GenerateToken(conn *GuacamoleLiteConnection, key, hmacKey []byte) (string, error) {
	var err error

	token, err := CreateGuacamoleLiteToken(conn, key, hmacKey)
	if err != nil {
		return "", err
	}
	return token, nil
}
