package main

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
)

// GuacamoleLiteConnectionWrapper ...
type GuacamoleLiteConnectionWrapper struct {
	Connection GuacamoleLiteConnection `json:"connection"`
}

// GuacamoleLiteConnection ...
type GuacamoleLiteConnection struct {
	Type     string                `json:"type"`
	Settings GuacamoleLiteSettings `json:"settings"`
}

// GuacamoleLiteSettings ...
// For ssh connection, guacd supports the following:
// 4.args,13.VERSION_1_3_0,8.hostname,8.host-key,4.port,8.username,8.password,9.font-name,9.font-size,11.enable-sftp,19.sftp-root-directory,21.sftp-disable-download,19.sftp-disable-upload,11.private-key,10.passphrase,12.color-scheme,7.command,15.typescript-path,15.typescript-name,22.create-typescript-path,14.recording-path,14.recording-name,24.recording-exclude-output,23.recording-exclude-mouse,22.recording-include-keys,21.create-recording-path,9.read-only,21.server-alive-interval,9.backspace,13.terminal-type,10.scrollback,6.locale,8.timezone,12.disable-copy,13.disable-paste,15.wol-send-packet,12.wol-mac-addr,18.wol-broadcast-addr,12.wol-udp-port,13.wol-wait-time
type GuacamoleLiteSettings struct {
	Hostname            string `json:"hostname"`
	Port                string `json:"port,omitempty"`
	Username            string `json:"username"`
	InstanceUsername    string `json:"instance_username"`
	Password            string `json:"password,omitempty"`
	PrivateKey          string `json:"private-key,omitempty"`
	Security            string `json:"security"`
	IgnoreCert          *bool  `json:"ignore-cert,omitempty"`
	EnableSFTP          *bool  `json:"enable-sftp,omitempty"`
	DisableSFTPDownload *int   `json:"sftp-disable-download,omitempty"`
	DisableSFTPUpload   *int   `json:"sftp-disable-upload,omitempty"`
	SFTPRootDirectory   string `json:"sftp-root-directory,omitempty"`
}

// GuacamoleLiteTokenObject ...
type GuacamoleLiteTokenObject struct {
	Iv    string `json:"iv"`
	Value string `json:"value"`
	HMAC  string `json:"hmac,omitempty"` // HMAC on Value (base64 encoded cipher text)
}

// CreateGuacamoleLiteToken creates a guacamole lite token
func CreateGuacamoleLiteToken(guacamoleLiteConnection *GuacamoleLiteConnection, encryptionKey, hmacKey []byte) (string, error) {
	//	Taken from https://github.com/vadimpronin/guacamole-lite
	//1. Generate initialization vector for encryption (iv).
	//2. Take json object with connection settings (see example above) and encrypt it using cyper and key from clientOptions.
	//3. Base64 encode result of p.2 (put it in value)
	//4. Create another json object containing {"iv": iv, "value": value}
	//5. Base64 encode result of p.4 and this will be your token

	//	Create random bytes
	iv := createRandomBytes(16)

	// Wrap
	gConnectionWrap := GuacamoleLiteConnectionWrapper{
		Connection: *guacamoleLiteConnection,
	}

	//	json dump the jsonConfig
	jsonConfig, err := json.Marshal(gConnectionWrap)
	if err != nil {
		return "", fmt.Errorf("failed to marshal guacamole lite connection object to JSON")
	}

	// Store the jsonConfig as a properly padded byte array
	paddedJSONText := pkcs5Padding(jsonConfig)

	// create the block
	block, err := aes.NewCipher(encryptionKey)
	if err != nil {
		return "", fmt.Errorf("failed to create an AES cipher block")
	}

	// create the cypher text
	ciphertext := make([]byte, len(paddedJSONText))
	// set the cypher mode
	mode := cipher.NewCBCEncrypter(block, iv)
	// encrypt
	mode.CryptBlocks(ciphertext, paddedJSONText)

	token := GuacamoleLiteTokenObject{
		Iv:    base64.StdEncoding.EncodeToString(iv),
		Value: base64.StdEncoding.EncodeToString(ciphertext),
	}
	if len(hmacKey) > 0 {
		if len(hmacKey) <= 32 {
			return "", fmt.Errorf("hmac key too short")
		}
		token.HMAC = generateHMAC([]byte(token.Value), hmacKey)
	}

	marshaledToken, err := json.Marshal(token)
	if err != nil {
		return "", fmt.Errorf("failed to marshal guacamole lite token object to JSON")
	}

	return base64.StdEncoding.EncodeToString(marshaledToken), nil
}

// createRandomBytes creates random bytes
func createRandomBytes(amt int) []byte {
	token := make([]byte, amt)
	rand.Read(token)
	return token
}

// pkcs5Padding pads the encryption to ensure proper placement
func pkcs5Padding(ciphertext []byte) []byte {
	padding := aes.BlockSize - len(ciphertext)%aes.BlockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func generateHMAC(data, key []byte) string {
	mac := hmac.New(sha256.New, key)
	mac.Write(data)

	macBase64 := base64.StdEncoding.EncodeToString(mac.Sum(nil))
	return macBase64
}
