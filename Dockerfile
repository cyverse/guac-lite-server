FROM node:22-alpine

LABEL org.label-schema.vcs-url="https://gitlab.com/cyverse/guac-lite-server"

COPY . /opt

# build frontend with basename /guacamole for BrowserRouter
ENV REACT_APP_BASEURL=/guacamole

# openssh for ssh-keygen
RUN apk add openssh
RUN cd /opt/client && npm ci && npm run build && rm -r ./node_modules/ ./src/ ./public/ ./package* ./README.md && \
    cd /opt && npm ci

EXPOSE 3000

WORKDIR /opt
CMD ["npm", "run", "start"]
