const fs = require("fs");
const path = require("path");
const { execSync } = require("child_process");

const KEYPATH = process.env.KEYPATH || "./private";

function privateKeyPath(username) {
    return `${KEYPATH + path.sep + username}`;
}

/**
 * read private key for a user
 * @param {string} username
 * @returns {string} private ssh key
 */
function readPrivateKey(username) {
    const keyPath = privateKeyPath(username);
    const content = fs.readFileSync(keyPath);
    return content.toString("utf-8").trimEnd();
}

/**
 * Create keypair for user if not exists, and respond with public key of user
 * @param {string} username
 * @param {Response} res
 */
function handlePublicKeyEndpoint(username, res) {
    let fileExists = false;
    try {
        fileExists = checkKeysExist(username);
    } catch (err) {
        console.error(err);
        res.status(500);
        res.render("Host OS File Error", {
            error: err,
        });
        return;
    }

    if (fileExists) {
        res.send(readPublicKey(username));
        return;
    }

    try {
        let cliOutput = generateKeyPairForUser(username);
        console.log(cliOutput);
    } catch (e) {
        res.status(500);
        res.render("Could not create SSH Key for USER", {
            error: e,
        });
        return;
    }

    try {
        res.send(readPublicKey(username));
    } catch (err) {
        console.error(err);
        res.status(500);
        res.render("Host OS File Error", {
            error: err,
        });
    }
}

// may throw exception
function checkKeysExist(username) {
    return (
        fs.existsSync(privateKeyPath(username)) && fs.existsSync(privateKeyPath(username) + ".pub")
    );
}

// may throw exception
function generateKeyPairForUser(username) {
    const keyPath = privateKeyPath(username);
    const cmd = `ssh-keygen -N '' -t rsa -b 4096 -m PEM -C ${username} -f ${keyPath}`;
    return execSync(cmd).toString(); // return cli output
}

function readPublicKey(username) {
    const keyPath = privateKeyPath(username) + ".pub";
    const content = fs.readFileSync(keyPath);
    return content.toString("utf-8").trimEnd();
}

function validateSSHKeyConfig() {
    if (!fs.existsSync(KEYPATH)) {
        throw new Error(`KEYPATH (${KEYPATH}) does not exists`);
    }
}

module.exports = { readPrivateKey, handlePublicKeyEndpoint, validateSSHKeyConfig };
