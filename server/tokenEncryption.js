const crypto = require("crypto");

/**
 * decrypt token into an object.
 *
 * - base64 decode the input string
 * - parse decoded string as json object
 * - decrypt the value property of the object
 * - parse the decrypted string as object
 * the plain text is expected to be json string.
 * @param clientOptions cryptography config
 * @param encodedString base64 encoded json string that contains IV and cipher text
 * @returns {any} the object parsed from decrypted plain text
 */
function decrypt(clientOptions, encodedString) {
    let encoded = JSON.parse(base64Decode(encodedString).toString("utf-8"));

    encoded.iv = base64Decode(encoded.iv);
    encoded.value = base64Decode(encoded.value);

    const decipher = crypto.createDecipheriv(
        clientOptions.crypt.cypher,
        clientOptions.crypt.key,
        encoded.iv
    );

    let decrypted = decipher.update(encoded.value, "binary", "ascii");
    decrypted += decipher.final("ascii");

    return JSON.parse(decrypted.toString());
}

/**
 * encrypt value (object) into a token.
 *
 * - serialized object into json string
 * - encrypt the json string and encode in base64
 * - construct an object with iv and value property, value property contains the base64 encoded encrypted json string
 * - serialized object from previous step into json string and encode in base64
 * @param clientOptions cryptography config
 * @param value object that contains guacamole-lite connection settings
 * @returns {string} base64 encoded token
 */
function encrypt(clientOptions, value) {
    const iv = crypto.randomBytes(16);
    const cipher = crypto.createCipheriv(clientOptions.crypt.cypher, clientOptions.crypt.key, iv);
    let cipherText = cipher.update(JSON.stringify(value), "utf8", "base64");
    cipherText += cipher.final("base64");
    const data = {
        iv: iv.toString("base64"),
        value: cipherText,
    };
    return base64Encode(JSON.stringify(data));
}

/**
 * base64 decode
 * @param string  base64 encoded
 * @returns {Buffer} data after decode
 */
function base64Decode(string) {
    return Buffer.from(string, "base64");
}

function base64Encode(input) {
    return Buffer.from(input).toString("base64");
}

module.exports = { decrypt, encrypt, base64Decode, base64Encode };
