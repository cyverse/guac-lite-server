// server/index.js
require("dotenv").config();
const path = require("path");
const express = require("express");
const GuacamoleLite = require("guacamole-lite");
const http = require("http");
const fs = require("fs");
const tokenencrypt = require("./tokenEncryption");
const { basicAuth, addAuthRoute, initAuth, validateAuthConfig } = require("./auth");
const { readPrivateKey, handlePublicKeyEndpoint, validateSSHKeyConfig } = require("./sshKey");

const CUSTOM_PORT = process.env.CUSTOM_PORT || 3000;
const BASEURL = process.env.BASEURL || "/";
const KEYPATH = process.env.KEYPATH || "./private";
const GUAC_ENCRYPTION_KEY_BASE64 = process.env.GUAC_ENCRYPTION_KEY;
const GUAC_LITE_AUTH_KEY = process.env.GUAC_LITE_AUTH_KEY;

validateConfig();
initAuth();

const app = express();
const server = http.createServer(app);

const clientOptions = {
    crypt: {
        cypher: "AES-256-CBC",
        key: tokenencrypt.base64Decode(GUAC_ENCRYPTION_KEY_BASE64),
    },
    log: {
        level: 30, // QUIET: 0, ERRORS: 10, NORMAL: 20, VERBOSE: 30, DEBUG: 40
    },
};

const guacLiteServer = createGuacamoleLiteServer(server, BASEURL, clientOptions);

// register SIGTERM for k8s grace termination
// https://cloud.google.com/blog/products/containers-kubernetes/kubernetes-best-practices-terminating-with-grace
process.on("SIGTERM", () => {
    guacLiteServer.close();
});

const baseRouter = express.Router();

addAuthRoute(baseRouter);

baseRouter.get("/health", (req, res) => {
    console.debug(`/health, connection count: ${guacLiteServer.connectionsCount}`);
    res.json({ message: "/health" });
});

baseRouter.get("/", requireAuth, async (req, res) => {
    console.log(`GET ${req.path}`);
    let token = req.query.token;
    if (!token) return res.status(400).send("Missing token parameter");

    // Decode connection token
    let decodedToken;
    try {
        decodedToken = tokenencrypt.decrypt(clientOptions, token);
        console.debug(decodedToken);
    } catch (e) {
        console.log(e);
        return res.status(400).send("Invalid token");
    }

    let tokenUpdated = false;
    if (!decodedToken?.connection?.type) {
        console.log("token is missing connection type");
        return res.status(400).send("Invalid token");
    }
    if (decodedToken?.connection?.type === "ssh") {
        // SSH connection requires injecting private ssh key
        if (!checkHasPrivateKey(decodedToken)) {
            console.log("token does not contain private key");
            try {
                decodedToken = injectPrivateKeyIntoConfig(decodedToken);
            } catch (e) {
                console.error(e);
                console.error(
                    `user ${decodedToken.connection?.settings?.username} has no private ssh key`
                );
                return res.status(400).send("user has no key");
            }
            tokenUpdated = true;
        }
    }

    if (decodedToken?.connection?.settings?.instance_username) {
        const connSettings = decodedToken.connection.settings;
        // If the instance_username (username to ssh into) is different from username (cacao username),
        // replace settings.username with settings.instance_username. settings.username will be consumed by
        // guacamole-lite to set up guacd connection.
        // This is in case that instance has a different username.
        if (connSettings.instance_username !== connSettings.username) {
            console.log("username is different from instance_username");
            decodedToken.connection.settings.username = connSettings.instance_username;
            tokenUpdated = true;
        }
    }

    if (tokenUpdated || !req.query.type) {
        // If token has been updated, we want to redirect user with updated token.
        // This (redirect) is a poor-mans-approach for passing data to browser.
        let reEncryptedToken = tokenencrypt.encrypt(clientOptions, decodedToken);
        console.log("redirect user with updated token");
        // pass connection type to frontend via query parameter, frontend need this information to
        // enable/disable functionality, getting it from query parameter is the easiest way currently
        res.redirect(
            path.join(BASEURL, req.path) +
                `?type=${decodedToken.connection.type}&token=${reEncryptedToken}`
        );
    } else {
        // render the page
        res.sendFile(path.resolve("./client/build/index.html"));
    }
});

/**
 * It is not critical for this endpoint to be secure, but we still hide it behind basic auth to deter random bots.
 * Example usage:
 * GET http://auth:7LdihMHs@127.0.0.1:3000/pubfor?username=randy
 */
baseRouter.get("/pubfor/", basicAuth("", GUAC_LITE_AUTH_KEY), ({ query }, res) => {
    const username = query.username;
    if (!username) {
        res.status(400).send("Missing username query parameter");
        return;
    }
    handlePublicKeyEndpoint(username, res);
});

// Have Node serve the files for our built React app
baseRouter.use(express.static(path.resolve(__dirname, "../client/build")));

// All other GET requests not handled before will return our React app
baseRouter.get("*", (req, res) => {
    console.log(`/* ${req.path}, ${req.query}`);
    res.sendFile(path.resolve(__dirname, "../client/build", "index.html"));
});

app.use(BASEURL, baseRouter);
server.listen(CUSTOM_PORT, () => {
    console.log(`listening on *:${CUSTOM_PORT}`);
});

function requireAuth(req, res, next) {
    console.debug("authenticated:", req.isAuthenticated());
    if (!req.isAuthenticated()) return res.redirect(`/login`);
    next();
}

function checkHasPrivateKey(decodedToken) {
    if (!decodedToken.connection) {
        return false;
    }
    if (!decodedToken.connection.settings) {
        return false;
    }
    if (!decodedToken.connection.settings["private-key"]) {
        return false;
    } else if (decodedToken.connection.settings["private-key"].length === 0) {
        return false;
    }
    return true;
}

function injectPrivateKeyIntoConfig(decodedToken) {
    let user = decodedToken.connection.settings.username;
    decodedToken.connection.settings["private-key"] = readPrivateKey(user);
    return decodedToken;
}

function validateConfig() {
    if (BASEURL[0] !== "/") {
        throw new Error("BASEURL needs to start with '/'");
    }
    if (BASEURL[BASEURL.length - 1] !== "/") {
        throw new Error("BASEURL needs to end with '/'");
    }

    if (isNaN(parseInt(CUSTOM_PORT))) {
        throw new Error("CUSTOM_PORT is not integer");
    }
    if (CUSTOM_PORT < 80 || CUSTOM_PORT > 65515) {
        throw new Error("CUSTOM_PORT out of range");
    }

    if (!fs.existsSync(KEYPATH)) {
        throw new Error(`KEYPATH (${KEYPATH}) does not exists`);
    }

    if (typeof GUAC_ENCRYPTION_KEY_BASE64 == "undefined") {
        throw new Error("GUAC_ENCRYPTION_KEY is missing");
    }
    let encryptionKey = tokenencrypt.base64Decode(GUAC_ENCRYPTION_KEY_BASE64);
    if (encryptionKey.length !== 32) {
        throw new Error("GUAC_ENCRYPTION_KEY must be 32-bytes/256-bits");
    }
    validateSSHKeyConfig();
    validateAuthConfig();
}

function createGuacamoleLiteServer(httpServer, baseURL, clientOptions) {
    const subPath = "guaclite"; // this needs to match the web socket sub-path declared in UI
    const webSocketOptions = {
        server: httpServer,
        path: `${baseURL}${subPath}`,
    };
    const guacdOptions = {
        host: "127.0.0.1",
        port: 4822,
    };
    return new GuacamoleLite(webSocketOptions, guacdOptions, clientOptions);
}
