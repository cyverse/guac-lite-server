const auth = require("./auth");

describe("http basic auth", () => {
    test("success", () => {
        // example from https://datatracker.ietf.org/doc/html/rfc7617#section-2
        const headerValue = "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==";
        expect(auth.parseBasicAuthHeader(headerValue)).toStrictEqual({
            username: "Aladdin",
            password: "open sesame",
        });
    });
    test("no type", () => {
        const headerValue = "QWxhZGRpbjpvcGVuIHNlc2FtZQ==";
        expect(() => {
            auth.parseBasicAuthHeader(headerValue);
        }).toThrow(Error);
    });
    test("no cred string", () => {
        const headerValue = "Basic";
        expect(() => {
            auth.parseBasicAuthHeader(headerValue);
        }).toThrow(Error);
    });
    test("not base64", () => {
        const headerValue = "Basic ABC123@@@@";
        expect(() => {
            auth.parseBasicAuthHeader(headerValue);
        }).toThrow(Error);
    });
    test("no colon", () => {
        const headerValue = "Basic YWJjZGVmZw=="; // abcdefg
        expect(() => {
            auth.parseBasicAuthHeader(headerValue);
        }).toThrow(Error);
    });
    test("start with colon", () => {
        const headerValue = "Basic OmFiY2RlZmc="; // :abcdefg
        expect(auth.parseBasicAuthHeader(headerValue)).toStrictEqual({
            username: "",
            password: "abcdefg",
        });
    });
    test("end with colon", () => {
        const headerValue = "Basic YWJjZGVmZzo="; // abcdefg:
        expect(auth.parseBasicAuthHeader(headerValue)).toStrictEqual({
            username: "abcdefg",
            password: "",
        });
    });
});
