const passport = require("passport");
const cookieSession = require("cookie-session");
const GlobusStrategy = require("passport-globus").Strategy;
const { OAuth2Strategy } = require("passport-oauth");
const KeycloakStrategy = require("@exlinc/keycloak-passport");
const { base64Decode } = require("./tokenEncryption");

const CILOGON_STRATEGY_NAME = "cilogon";
const GLOBUS_STRATEGY_NAME = "globus";
const KEYCLOAK_STRATEGY_NAME = "keycloak";

function getAuthProvider() {
    return process.env.AUTH_TYPE || CILOGON_STRATEGY_NAME;
}

const OAUTH2_CLIENT_ID = process.env.OAUTH2_CLIENT_ID;
const OAUTH2_CLIENT_SECRET = process.env.OAUTH2_CLIENT_SECRET;
const SESSION_SECRET = process.env.SESSION_SECRET;

/**
 * initialize authentication providers
 */
function initAuth() {
    const authProvider = getAuthProvider();
    console.log(`use ${authProvider} auth`);

    // These are needed for storing the user in the session
    passport.serializeUser(function (user, done) {
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        done(null, user);
    });

    switch (authProvider) {
        case GLOBUS_STRATEGY_NAME:
            initGlobus(OAUTH2_CLIENT_ID, OAUTH2_CLIENT_SECRET);
            break;
        case KEYCLOAK_STRATEGY_NAME:
            initKeycloak(OAUTH2_CLIENT_ID, OAUTH2_CLIENT_SECRET);
            break;
        case CILOGON_STRATEGY_NAME:
            initCILogon(OAUTH2_CLIENT_ID, OAUTH2_CLIENT_SECRET);
            break;
        default:
            throw new Error("unsupported auth provider");
    }
}

function initCILogon(OAUTH2_CLIENT_ID, OAUTH2_CLIENT_SECRET) {
    console.log("Initializing CILogon auth");

    // Add authentication strategy
    passport.use(
        CILOGON_STRATEGY_NAME,
        new OAuth2Strategy(
            {
                clientID: OAUTH2_CLIENT_ID,
                clientSecret: OAUTH2_CLIENT_SECRET,
                callbackURL: "/callback",
                authorizationURL: "https://cilogon.org/authorize",
                tokenURL: "https://cilogon.org/oauth2/token",
            },
            function (accessToken, refreshToken, params, profile, done) {
                return done(null, { accessToken }); // stored in req.user object
            }
        )
    );
}

function initGlobus(OAUTH2_CLIENT_ID, OAUTH2_CLIENT_SECRET) {
    console.log("Initializing Globus auth");

    // Add authentication strategy
    passport.use(
        GLOBUS_STRATEGY_NAME,
        new GlobusStrategy(
            {
                state: true, // remove this if not using sessions
                clientID: OAUTH2_CLIENT_ID,
                clientSecret: OAUTH2_CLIENT_SECRET,
                callbackURL: `/callback`,
            },
            function (accessToken, refreshToken, params, profile, done) {
                return done(null, { accessToken }); // stored in req.user object
            }
        )
    );
}

function initKeycloak(OAUTH2_CLIENT_ID, OAUTH2_CLIENT_SECRET) {
    console.log("Initializing Keycloak auth");

    const KEYCLOAK_URL = process.env.KEYCLOAK_URL;
    const KEYCLOAK_REALM = process.env.KEYCLOAK_REALM;

    // Add authentication strategy
    passport.use(
        KEYCLOAK_STRATEGY_NAME,
        new KeycloakStrategy(
            {
                host: KEYCLOAK_URL,
                realm: KEYCLOAK_REALM,
                clientID: OAUTH2_CLIENT_ID,
                clientSecret: OAUTH2_CLIENT_SECRET,
                callbackURL: "/callback",
                authorizationURL: `${KEYCLOAK_URL}/auth/realms/${KEYCLOAK_REALM}/protocol/openid-connect/auth`,
                tokenURL: `${KEYCLOAK_URL}/auth/realms/${KEYCLOAK_REALM}/protocol/openid-connect/token`,
                userInfoURL: `${KEYCLOAK_URL}/auth/realms/${KEYCLOAK_REALM}/protocol/openid-connect/userinfo`,
            },
            function (accessToken, refreshToken, params, profile, done) {
                return done(null, { accessToken }); // stored in req.user object
            }
        )
    );
}

/**
 * add auth route to the router
 * @param {Router} baseRouter
 */
function addAuthRoute(baseRouter) {
    const authProvider = getAuthProvider();

    // Initialize authentication
    baseRouter.use(
        cookieSession({
            name: "session",
            secret: SESSION_SECRET,
            maxAge: 48 * 60 * 60 * 1000, // 48 hours which appears to be the usual globus expiration time
        })
    );
    baseRouter.use(passport.initialize());
    baseRouter.use(passport.session());

    baseRouter.get(
        "/login",
        passport.authenticate(authProvider, {
            scope: [
                "email",
                "profile",
                "openid",
                // "https://auth.globus.org/scopes/b184f54f-4df1-45cd-8618-f6704bf1147c/all",
            ],
        })
    );

    baseRouter.get(
        "/callback",
        passport.authenticate(authProvider, {
            successRedirect: "/",
            failureRedirect: "/401",
        })
    );

    baseRouter.get("/401", (req, res) => {
        res.send("401 Unauthorized");
    });
}

/**
 * HTTP basic auth (username & password).
 * @param {string} expectedUsername
 * @param {string} expectedPassword
 * @returns {(function(*, *, *): void)|*}
 */
function basicAuth(expectedUsername, expectedPassword) {
    return function (req, res, next) {
        const authHeader = req.header("Authorization");
        if (!authHeader) {
            res.status(401)
                .header("WWW-Authenticate", `Basic realm="/pubfor"`)
                .json({ message: "Unauthorized" });
            return;
        }
        try {
            const cred = parseBasicAuthHeader(authHeader);
            // skip checking username if expectedUsername empty
            if (expectedUsername && cred.username !== expectedUsername) {
                res.status(401)
                    .header("WWW-Authenticate", `Basic realm="/pubfor"`)
                    .json({ message: "Unauthorized" });
                return;
            }
            if (cred.password !== expectedPassword) {
                res.status(401)
                    .header("WWW-Authenticate", `Basic realm="/pubfor"`)
                    .json({ message: "Unauthorized" });
                return;
            }
        } catch (e) {
            console.debug(e);
            res.status(401)
                .header("WWW-Authenticate", `Basic realm="/pubfor"`)
                .json({ message: "Unauthorized" });
            return;
        }
        // auth success, pass on the request to next
        next();
    };
}

function parseBasicAuthHeader(headerValue) {
    const headerValueSplits = headerValue.split(" ");
    if (headerValueSplits.length < 2) {
        throw new Error("Unauthorized");
    }
    if (headerValueSplits[0] !== "Basic") {
        throw new Error("Unauthorized, unsupported auth type");
    }
    const decoded = base64Decode(headerValueSplits[1]).toString("utf-8");
    const credSplits = decoded.split(":");
    if (credSplits.length < 2) {
        throw new Error("Unauthorized, invalid basic auth header");
    }
    return { username: credSplits[0], password: credSplits[1] };
}

function validateAuthConfig() {
    const authType = getAuthProvider();
    if (!authType) {
        throw new Error("missing AUTH_TYPE");
    }
    if (!process.env.OAUTH2_CLIENT_ID) {
        throw new Error("missing OAUTH2_CLIENT_ID");
    }
    if (!process.env.OAUTH2_CLIENT_SECRET) {
        throw new Error("missing OAUTH2_CLIENT_SECRET");
    }
    if (authType === KEYCLOAK_STRATEGY_NAME) {
        if (!process.env.KEYCLOAK_URL) {
            throw new Error("missing KEYCLOAK_URL");
        }
        if (!process.env.KEYCLOAK_REALM) {
            throw new Error("missing KEYCLOAK_REALM");
        }
    }
}

module.exports = { initAuth, addAuthRoute, basicAuth, parseBasicAuthHeader, validateAuthConfig };
