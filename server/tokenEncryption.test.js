const tokenEncryption = require("./tokenEncryption");
const { base64Encode, base64Decode } = require("./tokenEncryption");

test("token encryption", () => {
    const clientOptions = {
        crypt: {
            cypher: "AES-256-CBC",
            key: "abcdefghijklmnopqrstuvwxyz123456",
        },
    };

    let original = { foo: "bar", bar: { foo: 123 } };
    let token = tokenEncryption.encrypt(clientOptions, original);
    let decrypted = tokenEncryption.decrypt(clientOptions, token);
    expect(decrypted).toStrictEqual(original);
});

test("token decryption", () => {
    const clientOptions = {
        crypt: {
            cypher: "AES-256-CBC",
            key: base64Decode("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA="),
        },
    };

    let token =
        "eyJpdiI6ImxUUHVlVFZ2UmY4NGdjSmZlQnRkVkE9PSIsInZhbHVlIjoiMkRXKytySS9FMUR2K25wOXlIUy9hRGk2YzJpOUNZNnBPWmdDWDBHT3hqdG1GTEJkL1VDUHR5aE1nZDVmSW04SzdvWGtZdURpSi9ZUEJGRDBqcjhXQXA4RzNMMGNIWnRnSFh4TStDRVh5cXBkSFl1cnc3TU1Ob0hySWJkcHZrMHF1cnJCZTBKSjFwTGoxS0VCUE5xWjdkYmRCQURSajdPOUhiSFhyK1FJZ25qNzlabWs3TTFlTGlwVTlBREpUeWNiR2hSemNIOXN6aTZjbXJVaHBYa0kxVDJYVVVZckg5UFcrTktpN0lrUEkrWkNEL0J3Ri9JcnlWSlZnQVA4UHJlcHZnUmNLQ1ZSZ1czd082cXRrYmZRbWtCRVdTUnljbnhQajdIbVE1SkJodjJxOEZIVU40aHVuUlRZL1k2dEUzTEpGYWs2dGVmd2JRYU1XQlNZY1pEb3A3ckVFWEY3RU1MUnJQUllDeUVmR08wPSJ9";
    let decrypted = tokenEncryption.decrypt(clientOptions, token);
    const expected = {
        connection: {
            type: "ssh",
            settings: {
                hostname: "127.0.0.1",
                port: "22",
                username: "foobar",
                instance_username: "testuser123",
                security: "",
                "ignore-cert": false,
                "enable-sftp": true,
                "sftp-disable-download": 0,
                "sftp-disable-upload": 0,
                "sftp-root-directory": "/",
            },
        },
    };
    expect(decrypted).toStrictEqual(expected);
});

describe("base64", () => {
    test("encode decode", () => {
        let original = "abcdefghijklmnopqrstuvwxyz1234567890";
        let decoded = base64Decode(base64Encode(original)).toString();
        expect(decoded).toStrictEqual(original);
    });

    test("decode encode", () => {
        let original = "abcdefghijklmnopqrstuvwxyz1234567890";
        let encoded = base64Encode(base64Decode(original));
        expect(encoded).toStrictEqual(original);
    });
});
